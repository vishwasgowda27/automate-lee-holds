
# coding: utf-8

# In[ ]:

import cx_Oracle
import time
import numpy as np
import smtplib
from pandas import *
import pandas as pd
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from array import array
from IPython.display import HTML

#db connection
connection = cx_Oracle.connect("QV_RPT/ExQu1s1t3@fc8racp14n1-vip:1521/mfgsysp1.fc8racp14.gfoundries.com")
 
#email function   
def sendHtmlMail(df_html):
      email_subject = 'Today LEE Holds'
      email_from = "LEE_Holds@globalfoundries.com"
      email_to = ['vishwas.gowda@globalfoundries.com']
#       #email_cc = email_bcc = None
#       if 'email_cc' in dict_params and dict_params['email_cc']:
#             email_cc = dict_params['email_cc']
#       if 'email_bcc' in dict_params and dict_params['email_bcc']:
#             email_bcc = dict_params['email_bcc']
#       email_body = dict_params['email_body']

      msg = MIMEMultipart()
      message="Good Morning,\nPlease find today's list of LEE holds.\nHave a Good day"
      msg['To'] = ",".join(email_to)
      #msg['CC'] = email_cc
      msg['Subject'] = email_subject
    # add in the message body
      msg.attach(MIMEText(message, 'plain'))
      msg.attach(MIMEText(df_html, 'html')) 
      server = smtplib.SMTP('fc8smtp')
      toaddrs = [email_to]
      server.sendmail(email_from, email_to, msg.as_string())
      server.quit()
      print ("successfully sent email to %s:" % (msg['To']))
        
while True: 
query= """SELECT DISTINCT
    a.Lot_ID,
    a.Hold_Code,
    a.Hold_Time,
    a.HOLD_CLAIM_MEMO,
    round(extract( day from diff )*24+
              extract( hour from diff )+
              extract( minute from diff )/60, 2) as Hold_Hours,
              a.QTY,
              h.photo_layer,
              a.HOLD_USER_ID,
              a.Priority_Class as Priority,
    h.tech_id,
    h.PRODGRP_ID
    from (
    Select
    SYStimestamp - TO_TIMESTAMP (P.HOLD_TIME, 'YYYY-MM-DD-HH24.MI.SS.FF') as diff,
    HOLD_USER_ID,
    OPE_NO,
    HOLD_REASON_ID AS Hold_Code,
    Hold_Time,
    MAINPD_ID,
    Prodspec_ID,
    QTY,
    Priority_Class, 
    HOLD_CLAIM_MEMO, lot_id

    FROM SIVIEW_MMDB.FRLOT_HOLDRECORD@dbl_gemd P 
    JOIN SIVIEW_MMDB.FRLOT@dbl_gemd L ON P.D_THESYSTEMKEY = L.D_THESYSTEMKEY
    Where L.LOT_HOLD_STATE = 'ONHOLD'
    AND Lot_ID not Like 'T%'
    AND P.Hold_Reason_ID IN ('LEE', 'X-IP', 'X-JC', 'X-MC')
    --AND P.Hold_Reason_ID like 'LEE'
    AND L.LOT_STATE = 'ACTIVE'
    AND L.LOT_INV_STATE = 'OnFloor'
    and L.LOT_ID not like 'T%'  
    and L.LOT_INV_STATE not like 'NonProBank' 
    --and L.LOT_ID like '8XYT11089.026'
    ORDER BY HOLD_TIME DESC) a
    join SIVIEW_MMDB.FHOPEHS@dbl_gemd h on  a.LOT_ID=h.LOT_ID
    AND a.OPE_NO = h.OPE_NO 
    and photo_layer is not null
    order by Hold_TIME DESC, LOT_ID"""
pd.set_option('display.max_colwidth', -1)
    #pd.set_option('max_colwidth', 800)
df=pd.read_sql(query, con=connection)
    
df_html= df.to_html(index= False)
HTML(df_html)
sendHtmlMail(df_html)
time.sleep(1800)

